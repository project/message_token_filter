CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Installation
 * Usage

INTRODUCTION
------------

Current Maintainer: Kristofer Tengstrom <info@kristofer-tengstrom.se>

This module provides an input filter that replaces tokens for Message entities.

INSTALLATION
------------
This module is installed like any other Drupal module, for example through
the interface at yoursite.com/admin/modules.

The following dependencies are required:
- token
- message

USAGE
-----

Configure your input formats at yoursite.com/admin/config/content/formats.
This filter should be placed before any filters that might mess with tokens, such as HTML filter and Pathologic.
You may put it right at the top to be on the safe side.
